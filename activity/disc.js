let http = require("http");

//Mock Data for Users
let users = [
	
	{
		name: "Kim Dahyun",
		email: "kdahyun@gmail.com",
		password: "kdtwice9"
	},
	{
		name: "Lalisa Manoban",
		email: "lisabp@gmail.com",
		password: "blackpink4"
	}

];

//Mock Data for Courses
let courses = [
	
	{
		name: "PHP-Laravel 101",
		description: "Learn the basics of PHP and its framework, Laravel.",
		price: 25000
	},
	{
		name: "Python-Django 101",
		description: "Learn the basics of Python-Django.",
		price: 25000
	}

];

http.createServer((req,res)=>{

	if(req.url === '/' && req.method === "GET"){

		//what method of the response will allow us to add the necessary additional information or headers for our response?
		res.writeHead(200,{'Content-Type': 'text/plain'});

		//How can we end the response and send our response to the client?
		res.end("Hello from our new server!")
	}

	if(req.url === '/greeting' && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Hello this is our new greeting!');
	}

	if(req.url === '/' && req.method === "POST"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This route will allow us to create a new document.');
	}

	if(req.url === '/greeting' && req.method === "POST"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This route checks the /greeting endpoint and is a POST method request.');
	}

		if(req.url === '/' && req.method === "PUT"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This is a PUT method request. This route can be used to update documents in our database.');
	}

	if(req.url === '/' && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This is a DELETE method request. This route can be used to delete documents in our database.');
	}

	//route to GET users array
	if(req.url === '/users' && req.method === "GET"){
		//Content-Type: application/json, allows us the client to "know" that the incoming response is in JSON format.
		res.writeHead(200,{'Content-Type': 'application/json'});
		//res.end() only allows strings to be passed, so, we will stringify our array first into proper JSON format.
		res.end(JSON.stringify(users));
	}

	//route to POST a new user in the users array with details coming from our client.
	if(req.url === '/users' && req.method === "POST"){

		let requestBody = "";

		req.on('data', function(data){
			//console.log(data);
			//we save the stream of data into our variable.
			requestBody += data;
		});

		req.on('end', function(){

			//Once, the end of data stream is done, the requestBody variable now contains the JSON sent from our client.
			//We have to turn the JSON into a proper JS object:
			//update the requestBody variable with a parsed version of itself.
			requestBody = JSON.parse(requestBody);

			//Push the parsed JSON into our users array
			users.push(requestBody);

			//send the updated users array as response:
			res.writeHead(200,{'Content-Type': 'application/json'})
			res.end(JSON.stringify(users));

		})

	}


	/*
		Mini-Activity:

		Create a route which will be able to send the courses array in the client:
			-endpoint: '/courses'
			-method: 'GET'
			-Add res.writeHead() to send the proper status code and header to the client
			-send the courses array to the client using res.end()

		-Take a screenshot of your response and send it in the hangouts.

	*/




	//route to GET courses array
	if(req.url === '/courses' && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));
	}


	/*
		Mini-Activity (Take Home):

		Create a route which will be able delete the last item in our courses array and send the updated array in the client:
			-endpoint: '/courses'
			-method: 'DELETE'
			-Add res.writeHead() to send the proper status code and header to the client
			-send the courses array to the client using res.end()

				-Stretch Goal
					Do not use an array method to delete the last item in the array.

		-Take a screenshot of your response and send it in the hangouts.

	*/

	if(req.url === "/courses" && req.method === "DELETE"){

		//courses.pop();

		//stretch goal:
		//You can actually update the .length property of the array and shorten the array.
		//courses.length = courses.length-1
		courses.length--;

		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));
	}

}).listen(4000);

console.log(`Server is running on localhost:4000`);